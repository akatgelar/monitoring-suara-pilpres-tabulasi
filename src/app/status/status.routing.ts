import { Routes } from '@angular/router';

import { StatusComponent } from './status.component';

export const StatusRoutes: Routes = [{
  path: '',
  component: StatusComponent,
  data: {
    breadcrumb: 'Status TPS',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
