import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { StatusComponent } from './status.component';
import { StatusRoutes } from './status.routing';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(StatusRoutes),
      SharedModule
  ],
  declarations: [StatusComponent]
})

export class StatusModule {}
