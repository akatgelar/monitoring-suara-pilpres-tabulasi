import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';

@Component({
  selector: 'app-root',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet>'
})
export class AppComponent {
  route: any;
  thisroute: any;

  constructor(private router: Router) {

    router.events.forEach((event: NavigationEvent) => {
      if (event instanceof NavigationStart) {
        const url = event.url;
        this.route = url.split('/');
        this.thisroute = this.route[1];

        console.log(this.getRoute);
        if (this.thisroute === '') {
          this.router.navigate(['/tabulasi']);
        }
      }
    });

  }

  get getRoute() {
    return this.thisroute;
  }

}
