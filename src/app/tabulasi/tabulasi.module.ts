import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TabulasiComponent } from './tabulasi.component';
import { TabulasiRoutes } from './tabulasi.routing';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TabulasiRoutes),
      SharedModule
  ],
  declarations: [TabulasiComponent]
})

export class TabulasiModule {}
