import { Routes } from '@angular/router';

import { TabulasiComponent } from './tabulasi.component';

export const TabulasiRoutes: Routes = [{
  path: '',
  component: TabulasiComponent,
  data: {
    breadcrumb: 'Tabulasi',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
