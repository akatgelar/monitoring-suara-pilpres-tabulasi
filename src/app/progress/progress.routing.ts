import { Routes } from '@angular/router';

import { ProgressComponent } from './progress.component';

export const ProgressRoutes: Routes = [{
  path: '',
  component: ProgressComponent,
  data: {
    breadcrumb: 'Progress',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
