
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { AdminLayoutComponent } from '../layouts/admin/admin-layout.component';
import {fadeInOutTranslate} from '../shared/elements/animation';
import { AngularFirestore, AngularFirestoreDocument  } from 'angularfire2/firestore';
import { isNgTemplate } from '@angular/compiler';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import 'd3';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: [
    '../../../node_modules/c3/c3.min.css',
    '../../assets/icon/SVG-animated/svg-weather.css',
    '../../assets/css/chartist.css',
    '../../../node_modules/nvd3/build/nv.d3.css'
  ],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInOutTranslate]
})

export class ProgressComponent implements OnInit {


  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  isCollapsedSideBar = 'no-block';
  toggleOn = true;
  windowWidth: number;
  thisroute: any;
  public htmlButton: string;

  private _data1: number;
  private _data2: number;
  private _tpsMasuk: number;
  private _tpsBelum: number;
  private _tpsTotal: number;
  private _dataPersen1: number;
  private _dataPersen2: number;
  public _dataLoaded: boolean;
  public _dataLoaded_detail: boolean;
  public discreteBarData: any;
  public discreteBarOptions: any;
  private _dataTable: any;
  private _dataTableTotal1: any;
  private _dataTableTotal2: any;
  private _dataTableTotal: any;

  private _kode_wilayah: string;
  private _nama_wilayah: string;
  private _wilayah_latitude: string;
  private _wilayah_longitude: string;
  private _validasi: any;
  private _validasi_non: any;

  now: number;
  public provinsi: Observable<any[]>;
  public kota: Observable<any[]>;
  public kecamatan: Observable<any[]>;
  public kelurahan: Observable<any[]>;
  public tps: Observable<any[]>;
  public _provinsi: string;
  public _kota: string;
  public _kecamatan: string;
  public _kelurahan: string;
  public _tps: string;
  public _header_title: string;
  myForm: FormGroup;

  constructor(
    public adminLayout: AdminLayoutComponent,
    public db: AngularFirestore) {

    adminLayout.setRoute();

    setInterval(() => {
      this.now = Date.now();
    }, 1);

    this.myForm = new FormGroup({
      validasi: new FormControl(),
      provinsi: new FormControl(),
      kota: new FormControl(),
      kecamatan: new FormControl(),
      kelurahan: new FormControl(),
      tps: new FormControl(),
    });

    this.myForm.get('validasi').setValue('false');
    this.myForm.get('provinsi').setValue('');
    this.myForm.get('kota').setValue('');
    this.myForm.get('kecamatan').setValue('');
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');

    this.getProvinsi();
    this._header_title = 'Nasional';
  }

  ngOnInit() {
    // load data
    this.getData('test_nasional', '0', 'validasi_non');
    this.getDataDetail('test_provinsi', '', 'validasi_non');
  }

  onClickedOutside(e: Event) {
    if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
        this.toggleOn = true;
        this.verticalNavType = 'offcanvas';
    }
  }

  getProvinsi() {
    this.provinsi = this.db.collection('/test_provinsi', ref => ref.orderBy('nama_provinsi', 'asc')).valueChanges();
  }

  getKota(kode_provinsi) {
    this.kota = this.db.collection('/test_kota', ref => ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc')).valueChanges();
  }

  getKecamatan(kode_kota) {
    this.kecamatan = this.db.collection('/test_kecamatan', ref => ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc')).valueChanges();
  }

  getKelurahan(kode_kecamatan) {
    this.kelurahan = this.db.collection('/test_kelurahan', ref => ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc')).valueChanges();
  }

  getTPS(kode_kelurahan) {
    this.tps = this.db.collection('/test_tps', ref => ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc')).valueChanges();
  }

  onProvinsiChange(event) {
    this.kota = null;
    this.kecamatan = null;
    this.kelurahan = null;
    this.tps = null;
    this.myForm.get('kota').setValue('');
    this.myForm.get('kecamatan').setValue('');
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');

    const selectElementText = event.target['options']
      [event.target['options'].selectedIndex].text;
    if (selectElementText !== '-- Pilih --') {
      this._provinsi = selectElementText;
    } else {
      this._provinsi = null;
    }
    this._kota = null;
    this._kecamatan = null;
    this._kelurahan = null;
    this._tps = null;

    this.getKota(event.target.value);
  }

  onKotaChange(event) {
    this.kecamatan = null;
    this.kelurahan = null;
    this.tps = null;
    this.myForm.get('kecamatan').setValue('');
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');

    const selectElementText = event.target['options']
      [event.target['options'].selectedIndex].text;
    if (selectElementText !== '-- Pilih --') {
      this._kota = selectElementText;
    } else {
      this._kota = null;
    }
    this._kecamatan = null;
    this._kelurahan = null;
    this._tps = null;

    this.getKecamatan(event.target.value);
  }

  onKecamatanChange(event) {
    this.kelurahan = null;
    this.tps = null;
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');

    const selectElementText = event.target['options']
      [event.target['options'].selectedIndex].text;
    if (selectElementText !== '-- Pilih --') {
      this._kecamatan = selectElementText;
    } else {
      this._kecamatan = null;
    }
    this._kelurahan = null;
    this._tps = null;

    this.getKelurahan(event.target.value);
  }

  onKelurahanChange(event) {
    this.tps = null;
    this.myForm.get('tps').setValue('');

    const selectElementText = event.target['options']
      [event.target['options'].selectedIndex].text;
    if (selectElementText !== '-- Pilih --') {
      this._kelurahan = selectElementText;
    } else {
      this._kelurahan = null;
    }
    this._tps = null;

    this.getTPS(event.target.value);
  }

  onTPSChange(event) {
    const selectElementText = event.target['options']
      [event.target['options'].selectedIndex].text;
    if (selectElementText !== '-- Pilih --') {
      this._tps = selectElementText;
    } else {
      this._tps = null;
    }
  }

  onSubmit() {
    const form_value = this.myForm.value;
    const form_json = JSON.stringify(form_value);
    console.log(form_json);

    let validasi = 'validasi_non';
    if (form_value['validasi'] === 'true') {
      validasi = 'validasi';
    }else {
      validasi = 'validasi_non';
    }

    if (form_value['tps'] !== '') {

      this.getDataTPS('test_tps', form_value['tps'], validasi);
      this.getDataTPSSatuan('test_tps', form_value['tps'], validasi);
      this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan + ' - ' + this._tps;

    }else if (form_value['kelurahan'] !== '') {

      this.getData('test_kelurahan', form_value['kelurahan'], validasi);
      this.getDataTPSDetail('test_tps', form_value['kelurahan'], validasi);
      this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan;

    }else if (form_value['kecamatan'] !== '') {

      this.getData('test_kecamatan', form_value['kecamatan'], validasi);
      this.getDataDetail('test_kelurahan', form_value['kecamatan'], validasi);
      this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan;

    }else if (form_value['kota'] !== '') {

      this.getData('test_kota', form_value['kota'], validasi);
      this.getDataDetail('test_kecamatan', form_value['kota'], validasi);
      this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota;

    }else if (form_value['provinsi'] !== '') {

      this.getData('test_provinsi', form_value['provinsi'], validasi);
      this.getDataDetail('test_kota', form_value['provinsi'], validasi);
      this._header_title = 'PROV ' + this._provinsi;

    }else {

      this.getData('test_nasional', '0', validasi);
      this.getDataDetail('test_provinsi', '0', validasi);
      this._header_title = 'Nasional';

    }
  }

  getData(level, kode, validasi) {
    //  get Data
    this.db.collection('/' + level).doc(kode)
    .valueChanges()
    .subscribe(item => {
      console.log(item);

      this._kode_wilayah = '';
      this._nama_wilayah = '';

      if (level === 'test_provinsi') {
        this._kode_wilayah = item['kode_provinsi'];
        this._nama_wilayah = item['nama_provinsi'];
      } else if (level === 'test_kota') {
        this._kode_wilayah = item['kode_provinsi'];
        this._nama_wilayah = item['nama_provinsi'];
      } else if (level === 'test_kecamatan') {
        this._kode_wilayah = item['kode_kecamatan'];
        this._nama_wilayah = item['nama_kecamatan'];
      } else if (level === 'test_kelurahan') {
        this._kode_wilayah = item['kode_kelurahan'];
        this._nama_wilayah = item['nama_kelurahan'];
      } else if (level === 'test_tps') {
        this._kode_wilayah = item['kode_tps'];
        this._nama_wilayah = item['nama_tps'];
      }

      this._wilayah_latitude = item['nasional_latitude'];
      this._wilayah_longitude = item['nasional_longitude'];
      this._validasi = item['validasi'];
      this._validasi_non = item['validasi_non'];

      // change chart
      this._data1 = item[validasi]['suara_1'];
      this._data2 = item[validasi]['suara_2'];
      this._tpsMasuk = item[validasi]['tps_masuk'];
      this._tpsBelum = item[validasi]['tps_belum'];
      this._tpsTotal = item[validasi]['tps_total'];
      this._dataPersen1 = item[validasi]['suara_1'] / item[validasi]['suara_total'] * 100;
      this._dataPersen2 = item[validasi]['suara_2'] / item[validasi]['suara_total'] * 100;
      this._dataLoaded = true;
      this.changeChart(this._data1, this._data2, this._dataPersen1, this._dataPersen2);
    });

  }

  getDataDetail(level, kode, validasi) {
    const table_json = [];
    let total1 = 0;
    let total2 = 0;
    let total = 0;

    // if (kode === '') {
    //  get Data
    this.db.collection('/' + level)
    .valueChanges()
    .subscribe(item => {
      console.log(item);

      item.forEach(it => {
        const temp = {};

        temp['kode_wilayah'] = '';
        temp['nama_wilayah'] = '';

        if (level === 'test_provinsi') {
          temp['kode_wilayah'] = it['kode_provinsi'];
          temp['nama_wilayah'] = it['nama_provinsi'];
        } else if (level === 'test_kota') {
          temp['kode_wilayah'] = it['kode_kota'];
          temp['nama_wilayah'] = it['nama_kota'];
        } else if (level === 'test_kecamatan') {
          temp['kode_wilayah'] = it['kode_kecamatan'];
          temp['nama_wilayah'] = it['nama_kecamatan'];
        } else if (level === 'test_kelurahan') {
          temp['kode_wilayah'] = it['kode_kelurahan'];
          temp['nama_wilayah'] = it['nama_kelurahan'];
        } else if (level === 'test_tps') {
          temp['kode_wilayah'] = it['kode_tps'];
          temp['nama_wilayah'] = it['nama_tps'];
        }

        temp['suara_1'] = it[validasi]['suara_1'];
        temp['suara_2'] = it[validasi]['suara_2'];
        temp['tps_masuk'] = it[validasi]['tps_masuk'];
        temp['tps_belum'] = it[validasi]['tps_belum'];
        temp['tps_total'] = it[validasi]['tps_total'];
        temp['tps_persen'] = (Number(it[validasi]['tps_masuk']) / Number(it[validasi]['tps_total'])) * 100;
        temp['suara_total'] = it[validasi]['suara_1'] + it[validasi]['suara_2'];
        total1 = total1 + temp['suara_1'];
        total2 = total2 + temp['suara_2'];
        total = total + total1 + total2;
        table_json.push(temp);
      });

      this._dataLoaded_detail = true;
      this._dataTable = table_json;
      this._dataTableTotal1 = total1;
      this._dataTableTotal2 = total2;
      this._dataTableTotal = total;
    });
    // }

  }

  getDataTPS(level, kode, validasi) {

    //  get Data
    this.db.collection('/' + level).doc(kode)
    .valueChanges()
    .subscribe(item => {
      console.log(item);

      this._kode_wilayah = item['kode_tps'];
      this._nama_wilayah = item['nama_tps'];
      this._wilayah_latitude = item['nasional_latitude'];
      this._wilayah_longitude = item['nasional_longitude'];

      // change chart
      this._data1 = Number(item['suara_1']);
      this._data2 = Number(item['suara_2']);
      this._dataPersen1 = Number(item['suara_1']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
      this._dataPersen2 = Number(item['suara_2']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
      this._dataLoaded = true;
      this.changeChart(this._data1, this._data2, this._dataPersen1, this._dataPersen2);
    });

  }

  getDataTPSDetail(level, kode, validasi) {

    const table_json = [];
    let total1 = 0;
    let total2 = 0;
    let total = 0;

    //  get Data
    this.db.collection('/' + level)
    .valueChanges()
    .subscribe(item => {
      console.log(item);

      item.forEach(it => {
        const temp = {};

        temp['kode_wilayah'] = it['kode_tps'];
        temp['nama_wilayah'] = it['nama_tps'];
        temp['suara_1'] = it['suara_1'];
        temp['suara_2'] = it['suara_2'];
        if (temp['suara_1'] !== 0 && temp['suara_2'] !== 0) {
          temp['tps_masuk'] = 1;
        } else {
          temp['tps_masuk'] = 0;
        }
        temp['tps_total'] = 1;
        temp['tps_persen'] = (Number(temp['tps_masuk']) / Number(temp['tps_total'])) * 100;
        temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
        total1 = total1 + Number(temp['suara_1']);
        total2 = total2 + Number(temp['suara_2']);
        total = Number(total1) + Number(total2);
        table_json.push(temp);
      });

      this._dataLoaded_detail = true;
      this._dataTable = table_json;
      this._dataTableTotal1 = total1;
      this._dataTableTotal2 = total2;
      this._dataTableTotal = total;

    });

  }

  getDataTPSSatuan(level, kode, validasi) {

    const table_json = [];
    let total1 = 0;
    let total2 = 0;
    let total = 0;

    //  get Data
    this.db.collection('/' + level).doc(kode)
    .valueChanges()
    .subscribe(item => {
      console.log(item);

      const it = item;
      const temp = {};

      temp['kode_wilayah'] = it['kode_tps'];
      temp['nama_wilayah'] = it['nama_tps'];
      temp['suara_1'] = it['suara_1'];
      temp['suara_2'] = it['suara_2'];
      temp['tps_masuk'] = it['tps_masuk'];
      temp['tps_belum'] = it['tps_belum'];
      temp['tps_total'] = it['tps_total'];
      if (temp['suara_1'] !== 0 && temp['suara_2'] !== 0) {
        temp['tps_masuk'] = 1;
      } else {
        temp['tps_masuk'] = 0;
      }
      temp['tps_total'] = 1;
      temp['tps_persen'] = (Number(temp['tps_masuk']) / Number(temp['tps_total'])) * 100;
      temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
      total1 = temp['suara_1'];
      total2 = temp['suara_2'];
      total = temp['suara_total'];
      table_json.push(temp);

      this._dataLoaded_detail = true;
      this._dataTable = table_json;
      this._dataTableTotal1 = total1;
      this._dataTableTotal2 = total2;
      this._dataTableTotal = total;
    });

  }

  changeChart(data1: number, data2: number, dataPersen1: number, dataPersen2: number) {

    this.discreteBarOptions = {
      chart: {
          type: 'discreteBarChart',
          height: 500,
          // width: 600,
          margin : {
            top: 50,
            right: 50,
            bottom: 50,
            left: 50
          },
          x: function (d) {
              return d.label;
          },
          y: function (d) {
              return d.value;
          },
          xAxis: {
            showMaxMin: false,
            tickPadding: 15
          },
          yAxis: {
            tickFormat: function(d){
                return d3.format(',.0f')(d);
            },
            tickPadding: 10
          },
          valueFormat : d3.format(',.0f'),
          staggerLabels: false,
          showValues: true,
          tooltips : true
      }
    };

    this.discreteBarData = [{
        key: 'Cumulative Return',
        values: [{
            'label': 'Joko Widodo - KH Ma\'ruf Amin',
            'value': data1,
            'color': '#DA201B'
        }, {
            'label': 'Prabowo Subianto - Sandiaga Uno',
            'value': data2,
            'color': '#00A14D'
        }]
    }];
  }


  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  numberPercen(x) {
    return Number(x).toFixed(2);
  }

}
