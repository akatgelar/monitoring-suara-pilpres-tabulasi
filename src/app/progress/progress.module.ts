import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ProgressComponent } from './progress.component';
import { ProgressRoutes } from './progress.routing';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ProgressRoutes),
      SharedModule
  ],
  declarations: [ProgressComponent]
})

export class ProgressModule {}
