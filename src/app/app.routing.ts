import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'tabulasi',
      pathMatch: 'full'
    }, {
      path: 'tabulasi',
      loadChildren: './tabulasi/tabulasi.module#TabulasiModule'
    }, {
      path: 'progress',
      loadChildren: './progress/progress.module#ProgressModule'
    }, {
      path: 'status',
      loadChildren: './status/status.module#StatusModule'
    }
  ]
}, {
  path: '**',
  redirectTo: 'error/404'
}];
