import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import 'd3';
import 'nvd3';
import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';
import {CardRefreshDirective} from './card/card-refresh.directive';
import {CardToggleDirective} from './card/card-toggle.directive';
import { CardComponent } from './card/card.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ParentRemoveDirective} from './elements/parent-remove.directive';
import {PaginationModule} from 'ngx-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SpinnerComponent} from './spinner/spinner.component';
import {GoTopButtonModule} from 'ng2-go-top-button';
import {ScrollModule} from './scroll/scroll.module';
import {ToastyModule} from 'ng2-toasty';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {TagInputModule} from 'ngx-chips';
import {AgmCoreModule} from '@agm/core';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {ClickOutsideModule} from 'ng-click-outside';
import {NvD3Module} from 'ng2-nvd3';
// import {FilterService} from './service/filter.service';
import {DataService} from './data.service';
import {TabulasiComponent} from '../tabulasi/tabulasi.component';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PaginationModule.forRoot(),
      ScrollModule,
      ToastyModule.forRoot(),
      SimpleNotificationsModule.forRoot(),
      TagInputModule,
      AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'}),
      Ng2GoogleChartsModule,
      ClickOutsideModule,
      GoTopButtonModule,
      NvD3Module
  ],
  declarations: [
      AccordionAnchorDirective,
      AccordionLinkDirective,
      AccordionDirective,
      ToggleFullscreenDirective,
      CardRefreshDirective,
      CardToggleDirective,
      ParentRemoveDirective,
      CardComponent,
      SpinnerComponent
  ],
  exports: [
      AccordionAnchorDirective,
      AccordionLinkDirective,
      AccordionDirective,
      ToggleFullscreenDirective,
      CardRefreshDirective,
      CardToggleDirective,
      ParentRemoveDirective,
      CardComponent,
      SpinnerComponent,
      NgbModule,
      PaginationModule,
      FormsModule,
      ReactiveFormsModule,
      ScrollModule,
      ToastyModule,
      SimpleNotificationsModule,
      TagInputModule,
      AgmCoreModule,
      Ng2GoogleChartsModule,
      ClickOutsideModule,
      GoTopButtonModule,
      NvD3Module
  ],
  providers: [
      MenuItems,
      DataService,
    //   FilterService
      TabulasiComponent
  ]
})
export class SharedModule { }
