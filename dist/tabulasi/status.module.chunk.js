webpackJsonp(["status.module"],{

/***/ "../../../../../src/app/status/status.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"pcoded-wrapper\">\r\n    <nav id=\"main_navbar\" class=\" pcoded-navbar\" navbar-theme=\"theme\" active-item-theme=\"theme\" sub-item-theme=\"theme\" active-item-style=\"style6\" pcoded-navbar-position=\"fixed\" (clickOutside)=\"onClickedOutside($event)\" [exclude]=\"'#mobile-collapse'\">\r\n      <div class=\"sidebar_toggle\"><a href=\"javascript:;\"><i class=\"icon-close icons\"></i></a></div>\r\n      <div class=\"pcoded-inner-navbar main-menu\" appAccordion slimScroll width=\"100%\" height=\"100%\" size=\"4px\" color=\"#fff\" opacity=\"0.3\" allowPageScroll=\"false\">\r\n        <div class=\"\">\r\n          <div class=\"main-menu-header\">\r\n            <div class=\"user-details\">\r\n              <span><i class=\"icofont icofont-ui-calendar\" style=\"padding-right: 5px; font-size: 15px;\"></i> {{ now | date:'dd MMMM yyyy '}}</span>\r\n              <span><i class=\"icofont icofont-ui-clock\" style=\"padding-right: 5px; font-size: 15px;\"></i> {{ now | date:'HH:mm:ss'}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div>\r\n          <form class=\"md-float-material\" [formGroup]=\"myForm\" (ngSubmit)=\"onSubmit()\">\r\n          <!-- <form class=\"md-float-material\" (ngSubmit)=\"onSubmit()\"> -->\r\n          <div class=\"card table-card\">\r\n            <div class=\"col-sm-12 card-block\" style=\"padding-top: 10px;padding-bottom: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 5px;\">\r\n                    <h4 class=\"sub-title\">Status Validasi</h4>\r\n                </div> \r\n                <div class=\"col-sm-12\">\r\n                  <div class=\"form-radio\" id=\"radio-group\"> \r\n                      <div class=\"radio radio-inline\">\r\n                        <label>\r\n                          <input type=\"radio\" name=\"validasi\" formControlName=\"validasi\" value=\"true\">\r\n                          <i class=\"helper\"></i>Sudah Validasi\r\n                        </label>\r\n                      </div>\r\n                      <div class=\"radio radio-inline\">\r\n                        <label>\r\n                          <input type=\"radio\" name=\"validasi\" formControlName=\"validasi\" value=\"false\" checked=\"checked\">\r\n                          <i class=\"helper\"></i>Belum Validasi\r\n                        </label>\r\n                      </div> \r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card table-card\">\r\n            <div class=\"col-sm-12 card-block\" style=\"padding-top: 10px;padding-bottom: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Provinsi</h4>\r\n                  <select id=\"provinsi\" class=\"form-control\" formControlName=\"provinsi\" (change)=\"onProvinsiChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let prov of provinsi | async\" value=\"{{prov.kode_provinsi}}\">{{prov.nama_provinsi}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Kota</h4>\r\n                  <select id=\"kota\" class=\"form-control\" formControlName=\"kota\" (change)=\"onKotaChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let kot of kota | async\" value=\"{{kot.kode_kota}}\">{{kot.nama_kota}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Kecamatan</h4>\r\n                  <select id=\"kecamatan\" class=\"form-control\" formControlName=\"kecamatan\" (change)=\"onKecamatanChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let kec of kecamatan | async\" value=\"{{kec.kode_kecamatan}}\">{{kec.nama_kecamatan}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Kelurahan</h4>\r\n                  <select id=\"kelurahan\" class=\"form-control\" formControlName=\"kelurahan\" (change)=\"onKelurahanChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let kel of kelurahan | async\" value=\"{{kel.kode_kelurahan}}\">{{kel.nama_kelurahan}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">TPS</h4>\r\n                  <select id=\"tps\" class=\"form-control\" formControlName=\"tps\" (change)=\"onTPSChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let tp of tps | async\" value=\"{{tp.kode_tps}}\">{{tp.nama_tps}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">  \r\n                    <button type=\"submit\" class=\"btn btn-info btn-sm txt-white p-t-10 p-b-10\" style=\"width:100%\"><i class=\"icofont icofont-ui-search\"></i>Tampilkan</button>  \r\n  \r\n                    <!-- <button type=\"button\" class=\"btn btn-info btn-sm txt-white p-t-10 p-b-10\" style=\"width:100%\" (click)=\"changeData()\"><i class=\"icofont icofont-ui-search\"></i>Change</button>  \r\n                    <button type=\"button\" class=\"btn btn-info btn-sm txt-white p-t-10 p-b-10\" style=\"width:100%\" (click)=\"readData()\"><i class=\"icofont icofont-ui-search\"></i>Read</button>   -->\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 20px;\"> \r\n                  <br>\r\n                  <br>\r\n                  <br>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </nav>\r\n    <div class=\"pcoded-content\">\r\n      <div class=\"pcoded-inner-content\">\r\n        <div class=\"main-body\">\r\n          <div class=\"page-wrapper\">\r\n            <!-- <app-title></app-title> -->\r\n            <!-- <app-breadcrumbs></app-breadcrumbs> -->\r\n            <div class=\"page-body\">\r\n              <!-- <router-outlet><app-spinner> -->\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-12 col-sm-12\">\r\n                    <app-card style=\"text-align: center;\">\r\n                      <h5>{{_header_title}}</h5>\r\n                    </app-card>\r\n                    <br/>\r\n                    <br/>\r\n                    <app-card [title]=\"'PROGRESS SUARA MASUK'\" [headerContent]=\"''\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                          <div class=\"progress\">\r\n                            <div class=\"progress-bar progress-bar-striped progress-bar-animated bg-success\" role=\"progressbar\" aria-valuenow=\"\" aria-valuemin=\"0\" aria-valuemax=\"100\" [style.width]=\"_tpsPersen + '%'\"></div>\r\n                            <span>{{numberPercen(_tpsPersen)}}%</span>\r\n                          </div>\r\n                        </div>\r\n              \r\n                        <div class=\"col-md-12\">\r\n                          <br>\r\n                          <br>\r\n                        </div>\r\n              \r\n                        <div class=\"col-md-12\">\r\n                          <div class=\"table-responsive\">\r\n                            <table class=\"table table table-bordered\">\r\n                              <thead style=\"background-color: #F5F5F5;\">\r\n                              <tr>\r\n                                <th colspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Perolehan Suara</th>\r\n                                <th colspan=\"3\" style=\"text-align: center;vertical-align: middle;\">Status TPS</th>\r\n                              </tr> \r\n                              <tr>\r\n                                <th style=\"text-align: center;vertical-align: middle;\">Joko Widodo -<br> KH Ma'ruf Amin</th>\r\n                                <th style=\"text-align: center;vertical-align: middle;\">Prabowo Subianto -<br> Sandiaga Uno</th>  \r\n                                <th style=\"text-align: center;vertical-align: middle;\">TPS Masuk</th>\r\n                                <th style=\"text-align: center;vertical-align: middle;\">TPS Belum</th> \r\n                                <th style=\"text-align: center;vertical-align: middle;\">Total TPS</th> \r\n                              </tr>\r\n                              </thead>\r\n                              <tbody style=\"text-align: center\">\r\n                              <tr>\r\n                                <td>{{_data1}}</td>\r\n                                <td>{{_data2}}</td>\r\n                                <td>{{_tpsMasuk}}</td>\r\n                                <td>{{_tpsBelum}}</td>\r\n                                <td>{{_tpsTotal}}</td> \r\n                              </tr>\r\n                              </tbody>\r\n                            </table>\r\n                          </div>\r\n                        </div>\r\n              \r\n                      </div>\r\n                    </app-card>\r\n              \r\n                    <div class=\"col-md-12\">\r\n                      <br>\r\n                      <br>\r\n                    </div>\r\n                    \r\n                    <app-card [title]=\"'DATA DETAIL'\" [headerContent]=\"''\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-md-12\">\r\n                          <h4> </h4>\r\n                          <div class=\"table-responsive\">\r\n                            <table class=\"table table table-bordered\">\r\n                              <thead style=\"background-color: #F5F5F5;\">\r\n                              <tr>\r\n                                <th rowspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Provinsi</th>\r\n                                <th colspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Perolehan Suara</th>\r\n                                <th colspan=\"3\" style=\"text-align: center;vertical-align: middle;\">Status TPS</th>\r\n                              </tr> \r\n                              <tr>\r\n                                <th style=\"text-align: center;vertical-align: middle;\">Joko Widodo -<br> KH Ma'ruf Amin</th>\r\n                                <th style=\"text-align: center;vertical-align: middle;\">Prabowo Subianto -<br> Sandiaga Uno</th>  \r\n                                <th style=\"text-align: center;vertical-align: middle;\">TPS Masuk</th>\r\n                                <th style=\"text-align: center;vertical-align: middle;\">TPS Belum</th> \r\n                                <th style=\"text-align: center;vertical-align: middle;\">Total TPS</th> \r\n                              </tr>\r\n                              </thead>\r\n                              <tbody style=\"text-align: center\"  *ngIf=\"_dataLoaded_detail\">\r\n                              <tr *ngFor=\"let data of _dataTable\">\r\n                                <th scope=\"row\">{{data.nama_wilayah}}</th>\r\n                                <td>{{data.suara_1}}</td>\r\n                                <td>{{data.suara_2}}</td>\r\n                                <td>{{data.tps_masuk}}</td>\r\n                                <td>{{data.tps_belum}}</td>\r\n                                <td>{{data.tps_total}}</td>\r\n                              </tr>\r\n                              </tbody>\r\n                            </table>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </app-card>\r\n              \r\n                  </div>\r\n                </div>\r\n              <!-- </app-spinner></router-outlet> -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div> \r\n\r\n "

/***/ }),

/***/ "../../../../../src/app/status/status.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layouts_admin_admin_layout_component__ = __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_firestore__ = __webpack_require__("../../../../angularfire2/firestore/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StatusComponent = (function () {
    function StatusComponent(adminLayout, db) {
        var _this = this;
        this.adminLayout = adminLayout;
        this.db = db;
        this.deviceType = 'desktop';
        this.verticalNavType = 'expanded';
        this.verticalEffect = 'shrink';
        this.isCollapsedMobile = 'no-block';
        this.isCollapsedSideBar = 'no-block';
        this.toggleOn = true;
        adminLayout.setRoute();
        setInterval(function () {
            _this.now = Date.now();
        }, 1);
        this.myForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormGroup"]({
            validasi: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            provinsi: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            kota: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            kecamatan: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            kelurahan: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
            tps: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"](),
        });
        this.myForm.get('validasi').setValue('false');
        this.myForm.get('provinsi').setValue('');
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        this._tpsPersen = 0;
        this.getProvinsi();
        this._header_title = 'Nasional';
    }
    StatusComponent.prototype.ngOnInit = function () {
        // load data
        this.getData('test_nasional', '0', 'validasi_non');
        this.getDataDetail('test_provinsi', '', 'validasi_non');
    };
    StatusComponent.prototype.onClickedOutside = function (e) {
        if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
            this.toggleOn = true;
            this.verticalNavType = 'offcanvas';
        }
    };
    StatusComponent.prototype.getProvinsi = function () {
        this.provinsi = this.db.collection('/test_provinsi', function (ref) { return ref.orderBy('nama_provinsi', 'asc'); }).valueChanges();
    };
    StatusComponent.prototype.getKota = function (kode_provinsi) {
        this.kota = this.db.collection('/test_kota', function (ref) { return ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc'); }).valueChanges();
    };
    StatusComponent.prototype.getKecamatan = function (kode_kota) {
        this.kecamatan = this.db.collection('/test_kecamatan', function (ref) { return ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc'); }).valueChanges();
    };
    StatusComponent.prototype.getKelurahan = function (kode_kecamatan) {
        this.kelurahan = this.db.collection('/test_kelurahan', function (ref) { return ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc'); }).valueChanges();
    };
    StatusComponent.prototype.getTPS = function (kode_kelurahan) {
        this.tps = this.db.collection('/test_tps', function (ref) { return ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc'); }).valueChanges();
    };
    StatusComponent.prototype.onProvinsiChange = function (event) {
        this.kota = null;
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._provinsi = selectElementText;
        }
        else {
            this._provinsi = null;
        }
        this._kota = null;
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;
        this.getKota(event.target.value);
    };
    StatusComponent.prototype.onKotaChange = function (event) {
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kota = selectElementText;
        }
        else {
            this._kota = null;
        }
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;
        this.getKecamatan(event.target.value);
    };
    StatusComponent.prototype.onKecamatanChange = function (event) {
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kecamatan = selectElementText;
        }
        else {
            this._kecamatan = null;
        }
        this._kelurahan = null;
        this._tps = null;
        this.getKelurahan(event.target.value);
    };
    StatusComponent.prototype.onKelurahanChange = function (event) {
        this.tps = null;
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kelurahan = selectElementText;
        }
        else {
            this._kelurahan = null;
        }
        this._tps = null;
        this.getTPS(event.target.value);
    };
    StatusComponent.prototype.onTPSChange = function (event) {
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._tps = selectElementText;
        }
        else {
            this._tps = null;
        }
    };
    StatusComponent.prototype.onSubmit = function () {
        var form_value = this.myForm.value;
        var form_json = JSON.stringify(form_value);
        console.log(form_json);
        var validasi = 'validasi_non';
        if (form_value['validasi'] === 'true') {
            validasi = 'validasi';
        }
        else {
            validasi = 'validasi_non';
        }
        if (form_value['tps'] !== '') {
            this.getDataTPS('test_tps', form_value['tps'], validasi);
            this.getDataTPSSatuan('test_tps', form_value['tps'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan + ' - ' + this._tps;
        }
        else if (form_value['kelurahan'] !== '') {
            this.getData('test_kelurahan', form_value['kelurahan'], validasi);
            this.getDataTPSDetail('test_tps', form_value['kelurahan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan;
        }
        else if (form_value['kecamatan'] !== '') {
            this.getData('test_kecamatan', form_value['kecamatan'], validasi);
            this.getDataDetail('test_kelurahan', form_value['kecamatan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan;
        }
        else if (form_value['kota'] !== '') {
            this.getData('test_kota', form_value['kota'], validasi);
            this.getDataDetail('test_kecamatan', form_value['kota'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota;
        }
        else if (form_value['provinsi'] !== '') {
            this.getData('test_provinsi', form_value['provinsi'], validasi);
            this.getDataDetail('test_kota', form_value['provinsi'], validasi);
            this._header_title = 'PROV ' + this._provinsi;
        }
        else {
            this.getData('test_nasional', '0', validasi);
            this.getDataDetail('test_provinsi', '0', validasi);
            this._header_title = 'Nasional';
        }
    };
    StatusComponent.prototype.getData = function (level, kode, validasi) {
        var _this = this;
        //  get Data
        this.db.collection('/' + level).doc(kode)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            _this._kode_wilayah = '';
            _this._nama_wilayah = '';
            if (level === 'test_provinsi') {
                _this._kode_wilayah = item['kode_provinsi'];
                _this._nama_wilayah = item['nama_provinsi'];
            }
            else if (level === 'test_kota') {
                _this._kode_wilayah = item['kode_provinsi'];
                _this._nama_wilayah = item['nama_provinsi'];
            }
            else if (level === 'test_kecamatan') {
                _this._kode_wilayah = item['kode_kecamatan'];
                _this._nama_wilayah = item['nama_kecamatan'];
            }
            else if (level === 'test_kelurahan') {
                _this._kode_wilayah = item['kode_kelurahan'];
                _this._nama_wilayah = item['nama_kelurahan'];
            }
            else if (level === 'test_tps') {
                _this._kode_wilayah = item['kode_tps'];
                _this._nama_wilayah = item['nama_tps'];
            }
            _this._wilayah_latitude = item['nasional_latitude'];
            _this._wilayah_longitude = item['nasional_longitude'];
            _this._validasi = item['validasi'];
            _this._validasi_non = item['validasi_non'];
            // change chart
            _this._data1 = item[validasi]['suara_1'];
            _this._data2 = item[validasi]['suara_2'];
            _this._tpsMasuk = item[validasi]['tps_masuk'];
            _this._tpsBelum = item[validasi]['tps_belum'];
            _this._tpsTotal = item[validasi]['tps_total'];
            _this._tpsPersen = (item[validasi]['tps_masuk'] / item[validasi]['tps_total']) * 100;
            _this._dataPersen1 = item[validasi]['suara_1'] / item[validasi]['suara_total'] * 100;
            _this._dataPersen2 = item[validasi]['suara_2'] / item[validasi]['suara_total'] * 100;
            _this._dataLoaded = true;
            _this.changeChart(_this._data1, _this._data2, _this._dataPersen1, _this._dataPersen2);
        });
    };
    StatusComponent.prototype.getDataDetail = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        // if (kode === '') {
        //  get Data
        this.db.collection('/' + level)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            item.forEach(function (it) {
                var temp = {};
                temp['kode_wilayah'] = '';
                temp['nama_wilayah'] = '';
                if (level === 'test_provinsi') {
                    temp['kode_wilayah'] = it['kode_provinsi'];
                    temp['nama_wilayah'] = it['nama_provinsi'];
                }
                else if (level === 'test_kota') {
                    temp['kode_wilayah'] = it['kode_kota'];
                    temp['nama_wilayah'] = it['nama_kota'];
                }
                else if (level === 'test_kecamatan') {
                    temp['kode_wilayah'] = it['kode_kecamatan'];
                    temp['nama_wilayah'] = it['nama_kecamatan'];
                }
                else if (level === 'test_kelurahan') {
                    temp['kode_wilayah'] = it['kode_kelurahan'];
                    temp['nama_wilayah'] = it['nama_kelurahan'];
                }
                else if (level === 'test_tps') {
                    temp['kode_wilayah'] = it['kode_tps'];
                    temp['nama_wilayah'] = it['nama_tps'];
                }
                temp['suara_1'] = it[validasi]['suara_1'];
                temp['suara_2'] = it[validasi]['suara_2'];
                temp['tps_masuk'] = it[validasi]['tps_masuk'];
                temp['tps_belum'] = it[validasi]['tps_belum'];
                temp['tps_total'] = it[validasi]['tps_total'];
                temp['tps_persen'] = (Number(it[validasi]['tps_masuk']) / Number(it[validasi]['tps_total'])) * 100;
                temp['suara_total'] = it[validasi]['suara_1'] + it[validasi]['suara_2'];
                total1 = total1 + temp['suara_1'];
                total2 = total2 + temp['suara_2'];
                total = total + total1 + total2;
                table_json.push(temp);
            });
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
        // }
    };
    StatusComponent.prototype.getDataTPS = function (level, kode, validasi) {
        var _this = this;
        //  get Data
        this.db.collection('/' + level).doc(kode)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            _this._kode_wilayah = item['kode_tps'];
            _this._nama_wilayah = item['nama_tps'];
            _this._wilayah_latitude = item['nasional_latitude'];
            _this._wilayah_longitude = item['nasional_longitude'];
            // change chart
            _this._data1 = Number(item['suara_1']);
            _this._data2 = Number(item['suara_2']);
            _this._dataPersen1 = Number(item['suara_1']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
            _this._dataPersen2 = Number(item['suara_2']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
            _this._dataLoaded = true;
            _this.changeChart(_this._data1, _this._data2, _this._dataPersen1, _this._dataPersen2);
        });
    };
    StatusComponent.prototype.getDataTPSDetail = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        //  get Data
        this.db.collection('/' + level)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            item.forEach(function (it) {
                var temp = {};
                temp['kode_wilayah'] = it['kode_tps'];
                temp['nama_wilayah'] = it['nama_tps'];
                temp['suara_1'] = it['suara_1'];
                temp['suara_2'] = it['suara_2'];
                if (temp['suara_1'] !== 0 && temp['suara_2'] !== 0) {
                    temp['tps_masuk'] = 1;
                }
                else {
                    temp['tps_masuk'] = 0;
                }
                temp['tps_total'] = 1;
                temp['tps_persen'] = (Number(temp['tps_masuk']) / Number(temp['tps_total'])) * 100;
                temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
                total1 = total1 + Number(temp['suara_1']);
                total2 = total2 + Number(temp['suara_2']);
                total = Number(total1) + Number(total2);
                table_json.push(temp);
            });
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
    };
    StatusComponent.prototype.getDataTPSSatuan = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        //  get Data
        this.db.collection('/' + level).doc(kode)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            var it = item;
            var temp = {};
            temp['kode_wilayah'] = it['kode_tps'];
            temp['nama_wilayah'] = it['nama_tps'];
            temp['suara_1'] = it['suara_1'];
            temp['suara_2'] = it['suara_2'];
            temp['tps_masuk'] = it['tps_masuk'];
            temp['tps_belum'] = it['tps_belum'];
            temp['tps_total'] = it['tps_total'];
            if (temp['suara_1'] !== 0 && temp['suara_2'] !== 0) {
                temp['tps_masuk'] = 1;
            }
            else {
                temp['tps_masuk'] = 0;
            }
            temp['tps_total'] = 1;
            temp['tps_persen'] = (Number(temp['tps_masuk']) / Number(temp['tps_total'])) * 100;
            temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
            total1 = temp['suara_1'];
            total2 = temp['suara_2'];
            total = temp['suara_total'];
            table_json.push(temp);
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
    };
    StatusComponent.prototype.changeChart = function (data1, data2, dataPersen1, dataPersen2) {
    };
    StatusComponent.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    };
    StatusComponent.prototype.numberPercen = function (x) {
        return Number(x).toFixed(2);
    };
    return StatusComponent;
}());
StatusComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-status',
        template: __webpack_require__("../../../../../src/app/status/status.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_angularfire2_firestore__["a" /* AngularFirestore */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angularfire2_firestore__["a" /* AngularFirestore */]) === "function" && _b || Object])
], StatusComponent);

var _a, _b;
//# sourceMappingURL=status.component.js.map

/***/ }),

/***/ "../../../../../src/app/status/status.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusModule", function() { return StatusModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__status_component__ = __webpack_require__("../../../../../src/app/status/status.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__status_routing__ = __webpack_require__("../../../../../src/app/status/status.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var StatusModule = (function () {
    function StatusModule() {
    }
    return StatusModule;
}());
StatusModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__status_routing__["a" /* StatusRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__status_component__["a" /* StatusComponent */]]
    })
], StatusModule);

//# sourceMappingURL=status.module.js.map

/***/ }),

/***/ "../../../../../src/app/status/status.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__status_component__ = __webpack_require__("../../../../../src/app/status/status.component.ts");

var StatusRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__status_component__["a" /* StatusComponent */],
        data: {
            breadcrumb: 'Status TPS',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }];
//# sourceMappingURL=status.routing.js.map

/***/ })

});
//# sourceMappingURL=status.module.chunk.js.map